import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput
} from 'react-native';
import styles from './styles';

const SearchBar = ({searchContact, value, setValue}) => {

  const onChangeText = (text) => {
    setValue(text);
    if (text!=='Search') {
      searchContact(text)
    }
  }

  const onBlur = () => {
    if (value.length===0) {
      setValue('Search')
    }
  }

  const onFocus = () => {
    if (value==='Search') {
      setValue('')
    }
  }

  return (
      <View>
        <TextInput
            onFocus = {onFocus}
            onBlur = {onBlur}
            style={styles.input}
            value={value}
            onChangeText={text => onChangeText(text)}
        />
      </View>
  )
};

export default SearchBar;
