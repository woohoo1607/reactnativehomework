import React, {useState} from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
} from 'react-native';
import styles from './styles';

const ContactForm = ({createNewContact, changeFormStatus}) => {
  let [name, setName] = useState('');
  let [surname, setSurname] = useState('');
  let [phone, setPhone] = useState('');
  let [email, setEmail] = useState('');

  const cancel = () => {
    setName('');
    setSurname('');
    setPhone('');
    setEmail('');
    changeFormStatus();
  }

  const createContact = () => {
    const newContact = {
      emailAddresses: [{
        label: 'home',
        email: email,
      }],
      displayName: `${name} ${surname}`,
      givenName: name,
      familyName: surname,
      phoneNumbers: [{
        label: 'mobile',
        number: phone,
      }],
    };
    createNewContact(newContact)
  };

  return (
      <View>
        <Text style={styles.label}>Name</Text>
        <TextInput
            style={styles.input}
            value={name}
            onChangeText={text => setName(text)}
        />
        <Text style={styles.label}>Surname</Text>
        <TextInput
            style={styles.input}
            value={surname}
            onChangeText={text => setSurname(text)}
        />
        <Text style={styles.label}>phone number</Text>
        <TextInput
            style={styles.input}
            value={phone}
            onChangeText={text => setPhone(text)}
        />
        <Text style={styles.label}>email</Text>
        <TextInput
            style={styles.input}
            value={email}
            onChangeText={text => setEmail(text)}
        />
        <View style={styles.buttonsContainer}>
          <Button title='Create contact' color='green' onPress={createContact}/>
          <Button title='cancel' color='red' onPress={cancel}/>
        </View>
      </View>
  )
};

export default ContactForm;
