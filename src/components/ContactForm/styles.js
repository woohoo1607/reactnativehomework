import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  input: {
    height: 40,
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  label: {
    marginTop: 5,
    color: 'gray',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 10,

  }
});

export default styles;
