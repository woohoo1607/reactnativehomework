import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  Button,
  BackHandler,
} from 'react-native';
import styles from './styles';
import Contact from "../Contact/Contact";
import ContactForm from "../ContactForm/ContactForm";
import Preloader from "../Preloader/Preloader";

const Home = ({phonebook, isFetching, createNewContact, deleteContact, setIsSearch, ...props}) => {

  useEffect(() => {
    setIsSearch(true);
  }, [])

  let [isViewForm, setIsViewForm] = useState(false)
  const changeFormStatus = () => {
    setIsViewForm(!isViewForm);
  }

  const backFunction = () => {
    props.history.goBack();
    return true
  }
  BackHandler.addEventListener('hardwareBackPress', backFunction);

  return (
    <View style={styles.container}>
      <ScrollView>
        {!isViewForm && <Button title='Add contact' onPress={changeFormStatus} />}
        {isViewForm &&<ContactForm createNewContact={createNewContact} changeFormStatus={changeFormStatus}/>}
        {phonebook.map((contact, i) => <Contact key={i} contact={contact} deleteContact={deleteContact}/> )}
        {isFetching && <Preloader />}
      </ScrollView>
    </View>
  )
};

export default Home;
