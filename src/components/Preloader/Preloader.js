import React from 'react';
import {View, Text} from 'react-native';

const Preloader = () => {
  return (
      <View style={{alignItems: 'center'}}>
        <Text>Loading...</Text>
      </View>
  )
};

export default Preloader;
