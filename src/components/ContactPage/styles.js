import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  imgContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15,
  },
  userInfo: {},
  label: {
    color: 'gray',
  },
  info: {
    fontSize: 20,
    marginBottom: 20,
    color: '#000',
  },
  iconButton: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    overflow: 'hidden',
  },
  call: {
    backgroundColor: 'green',
  },
  dlt: {
    backgroundColor: 'red',
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '60%',
    marginLeft: '20%',
    marginTop: 40,
  }
});

export default styles;
