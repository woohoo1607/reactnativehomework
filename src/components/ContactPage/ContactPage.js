import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Button,
} from 'react-native';
import Contacts from 'react-native-contacts';
import call from 'react-native-phone-call'
import Icon from 'react-native-vector-icons/MaterialIcons';
import NoAvatar from '../../img/noAvatar.png';
import styles from './styles';

const ContactPage = ({deleteContact, setIsSearch, ...props}) => {
  let [contact, setContact] = useState({});
  let [isFetching, setIsFetching] = useState(true);
  useEffect( () => {
    Contacts.getContactById(props.match.params.id, (err, contact) => {
      setContact(contact);
      setIsFetching(false);
      setIsSearch(false);
    })
  }, [])
  const dltContact = () => {
    deleteContact(props.match.params.id);
    props.history.goBack();
  };
  const calling = () => {
    call({number: contact.phoneNumbers[0].number, prompt: false})
  };

  return (
      <View>
        {!isFetching && <>
        <View style={styles.imgContainer}>
          <Image
            resizeMode={'contain'}
            style={styles.image}
            source={NoAvatar}
          />
        </View>
        <View style={styles.userInfo}>
          <Text style={styles.label}>Name</Text>
          <Text style={styles.info}>{contact.displayName}</Text>
          <Text style={styles.label}>Phone number</Text>
          <Text style={styles.info}>{contact.phoneNumbers[0].number}</Text>
        </View>
        </>}
        <View style={styles.btnContainer}>
          <View style={[styles.iconButton, styles.call]} >
            <Icon name='call' style={{fontSize: 26, color: '#FFF'}} onPress={calling} />
          </View>
          <View style={[styles.iconButton, styles.dlt]}>
            <Icon name='delete-sweep' style={{fontSize: 26, color: '#FFF'}} onPress={dltContact}/>
          </View>
        </View >
      </View>
  )
};

export default ContactPage;
