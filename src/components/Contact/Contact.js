import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Link} from "react-router-native";
import styles from './styles';

const Contact = ({contact, deleteContact}) => {
  const dltContact = (recordID) => () => {
    deleteContact(recordID);
  };
  return (
      <Link to={`contact/${contact.recordID}`}>
        <View style={styles.container}>
          <View style={{flexGrow: 30}}>
            <Text style={styles.title}>{contact.displayName}</Text>
            <Text style={styles.email}>{contact.emailAddresses[0].email}</Text>
          </View>
          <View style={{flexGrow: 1}}>
            <Icon name='delete-sweep' style={{fontSize: 26, color: 'red'}} onPress={dltContact(contact.recordID)}/>
          </View>
        </View>
      </Link>
  )
};

export default Contact;
