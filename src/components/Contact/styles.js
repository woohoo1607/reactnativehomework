import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#b3d9ff',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    padding: 5,
    flexDirection: 'row',
  },
  title: {
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
  },
  email: {
    fontSize: 15,
    color: '#888',
  },
});

export default styles;
