/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  PermissionsAndroid,
} from 'react-native';
import Contacts from 'react-native-contacts';
import {NativeRouter, nativeHistory, Route, useHistory} from 'react-router-native';
import Home from "./src/components/Home/Home";
import SearchBar from "./src/components/SearchBar/SearchBar";
import ContactPage from "./src/components/ContactPage/ContactPage";

const App: () => React$Node = () => {
  let [phonebook, setPhonebook] = useState([]);
  let [isFetching, setIsFetching] = useState(true);
  let [isSearch, setIsSearch] = useState(true);
  let [value, setValue] = useState('Search');
  const searchContact = (text) => {
    Contacts.getContactsMatchingString(text, (err, contacts) => {
      setPhonebook(contacts)
    });
  }
  const getList = () => {
    Contacts.getAll((err, contacts) => {
      if (err === 'denied') {
        console.log("cannot access");
      } else {
        setPhonebook(contacts)
        setIsFetching(false)
      }
    })
  };
  useEffect(() => {
    PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'This app would like to view your contacts.',
          'buttonPositive': 'Please accept bare mortal',
        }
    ).then(() => {
      getList();
    });
    PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
        {
          'title': 'Contacts',
          'message': 'This app would like to write your contacts.',
          'buttonPositive': 'Please accept bare mortal',
        }
    )
  }, []);

  const createNewContact = (newContact) => {
    Contacts.addContact(newContact, (err, contact) => {
      if (err) throw err;
      getList();
    });
  };

  const deleteContact = (recordID) => {
    Contacts.deleteContact({recordID: recordID}, (err, recordId) => {
      if (err) {
        throw err;
      }
      getList();
    })
  }

  return (
      <NativeRouter>
        <View style={styles.app}>
          {isSearch && <SearchBar searchContact={searchContact} value={value} setValue={setValue}/>}
          {!isFetching && <View style={styles.app}>
            <Route exact path="/" component={(props) => <Home isFetching={isFetching}
                                                              phonebook={phonebook}
                                                              createNewContact={createNewContact}
                                                              deleteContact={deleteContact}
                                                              setIsSearch={setIsSearch}
                                                               {...props}
                                              />}
            />
            <Route path="/contact/:id" component={(props) => <ContactPage deleteContact={deleteContact}
                                                                          setIsSearch={setIsSearch}
                                                                          {...props}
                                              />}
            />
          </View>}
        </View>
      </NativeRouter>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  app: {
    backgroundColor: '#fff',
    flex: 1,
    padding: 5
  },
});

export default App;
